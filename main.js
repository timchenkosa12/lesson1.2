/*
    Task 1:

    Необходимо создать информацию о себе, используя переменные, в которых будет:
    -ваше имя,
    -ваш возраст,  
    -поле с вашим статусом о семейном положении, замужем/женаты (Либо истина , либо ложь)
    -по аналогии с предыдущим, поле с детьми

    Также необходимо определить тип данных всех ваших полей и вывести результат в консоль
*/
let myName = 'Sergey',
    myAge = 35,
    notMarried = false,
    haveChildren = true;

    console.log(typeof myName);
    console.log(typeof myAge);
    console.log(typeof notMarried);
    console.log(typeof haveChildren);
/* 
    Task 2:

    Напишите скрипт, который находит площадь прямоугольника

    -высота 23см,
    -шириной 10см

    Каждая сущность должна находиться в своей переменной
*/
let height1 = 23,
    width1 = 10;
let areaRectangle = height1 * width1;
console.log(areaRectangle);    
/*
    Task 3:

    Напиши скрипт, который находит объем цилиндра
    
    -высота 10м  
    -площадь основания 4м

    Каждая сущность должна находиться в своей переменной
*/
let height2 = 10,
    baseArea = 4;
let cylinderVolume = baseArea * height2;
console.log(cylinderVolume);   
/*
    Task 4:

    Напиши рядом с каждым выражением , тот ответ который по вашему мнению выведет console.
    И потом сравните ваш результат с тем что на самом деле вывела консоль.
    
    Infinity - "1"
    "42" + 42
    2 + "1 1"
    99 + 101
    "1" - "1"
    "Result: " + 10/2
    3 + " bananas " + 2 + " apples "

*/
Infinity - "1"  //Infinity
    "42" + 42   //4242
    2 + "1 1"   //21 1
    99 + 101    //200
    "1" - "1"   //0
    "Result: " + 10/2 //Result:5
    3 + " bananas " + 2 + " apples " //3 bananas 2 apples " 
